import { useState } from "react";
import Navbar from "./components/Navbar";
import NavbarLink from "./components/NavbarLink";
import BackgroundImage from "./components/BackGroundImage";
import Text from "./components/Text";
import Section from "./components/Section";
import MediaObject from "./components/MediaObject";
import background from './images/landing.jpg'
import LogInLink from "./components/login/LogInLink";

function Landing() {
  const [isLogInHidden, setIsLogInHidden] = useState(false);
  const [userCount, setUserCount] = useState(5)
  
  const handleLogInClick = () => {
    setIsLogInHidden(true)
  }

  return (
    <>
    <Navbar height='15vh' width='100%'>
        <NavbarLink>
          <h2>Home</h2>
        </NavbarLink>
        <NavbarLink>
          <h2>About</h2>
        </NavbarLink>
        <NavbarLink>
          <h2>Contact</h2>
        </NavbarLink>
    </Navbar>
    <LogInLink display={isLogInHidden} handleLogInClick={handleLogInClick} userCount={userCount}></LogInLink>
    <BackgroundImage imgSrc={background} width='100%'>
        <Text size='xl'></Text>
    </BackgroundImage>
    <Section>
        <MediaObject content='Content 1' height='85%' width='25%'></MediaObject>
        <MediaObject content='Content 2' height='85%' width='25%'></MediaObject>
        <MediaObject content='Content 3' height='85%' width='25%'></MediaObject>
    </Section>
    </>
  );
}

export default Landing;
