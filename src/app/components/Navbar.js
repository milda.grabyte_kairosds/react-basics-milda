function Navbar({children}, props) {
  return (
    <>
        <nav style={{height: props.height, width: props.width,  display: 'flex', justifyContent: 'flex-end'}}>
            {children}
        </nav>
    </>
  );
}

export default Navbar;