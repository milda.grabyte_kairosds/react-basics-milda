function Section({children}, props) {
  return (
    <>
       <section style={{position: 'fixed', bottom: '0', height: '35%', width: '100%', display: 'flex', flexDirection: 'row', justifyContent: 'space-around'}}>
         {children}
         </section>
    </>
  );
}

export default Section;