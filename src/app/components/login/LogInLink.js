import MediaObject from "../MediaObject";

function LogInLink(props) {
    const handleLogInClick = () => {
        props.handleLogInClick();
    };
    const display = props.display ? 'none' : 'flex';
    const userCards = [...Array(props.userCount)].map((user, i) => <MediaObject key={i}></MediaObject>)
  return (
    <>
        <div onClick={handleLogInClick} style={{display: display, height: '20vh', width: '20vw', border: '3px solid #0E6CF9', margin: 'auto', marginTop: '40px'
        , flexDirection: 'column', justifyContent: 'center', textAlign: 'center'}}>
            <div type='button'>
                <h3 style={{fontFamily: "Lucida Console", fontSize: '50px'}}>Log in</h3>
            </div>
        </div>
        <div>
            {userCards}
        </div>
    </>
  );
}

export default LogInLink;