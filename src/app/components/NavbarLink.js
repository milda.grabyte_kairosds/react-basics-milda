function NavbarLink({children}) {
  return (
    <>
        <div style={{height:'80%', width:'15%', border: '3px solid salmon', display: 'flex', flexDirection: 'column', alignItems: 'center', marginRight:'10px'}}>
            {children}
        </div>
    </>
  );
}

export default NavbarLink;