function BackgroundImage(props) {
  return (
    <>
        <img className='background_image' src={props.imgSrc} alt='backgroundImage' style={{width: props.width, position: 'fixed', top: '0', left: '0', zIndex: '-2'}}></img> 
    </>
  );
}

export default BackgroundImage;