function MediaObject(props) {
  return (
    <>
        <div style={{height: props.height, width: props.width, border: '3px solid salmon', display: 'flex', flexDirection: 'column',alignItems: 'center'}}>
            <h2 style={{fontSize: '40px'}}>{props.content}</h2>
        </div>
    </>
  );
}

export default MediaObject;